package serveur;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

import application.MyPanel;
import application.Pane;

import java.io.IOException;

/**
 * Recevoir des informations sur les clients et ouvrir la planche �� dessin et transformer les donnees
 * graphes : une liste des tableau de double qui contient des donnee des graphes
 *
 */
public class ReceveurEnvoyeur extends Thread 
{
	Socket socket;
	int noClient;
	BufferedReader fluxEntrant;
	PrintStream fluxSortant;
	private MyPanel panel;
	private ArrayList<double[]> graphes;
	
	
	public ReceveurEnvoyeur(Socket interlocuteurClient,int noConnexion) throws IOException {
		this.socket = interlocuteurClient;
		this.fluxEntrant=new BufferedReader(new InputStreamReader(socket.getInputStream()));
		this.fluxSortant=new PrintStream(socket.getOutputStream());
		this.noClient =noConnexion;
		this.graphes = new ArrayList<double[]>();
	}
	
	
	
	@Override
	public void run()
	{
		try {
			System.out.println("Conversation demaree avec le client n"+this.noClient);
			
			// ouvrir la planche
			Pane frame = new Pane();
            frame.setVisible(true);
			panel = frame.getPanel();
			
			// recevoir les messages du client
			while(!this.isInterrupted()) 
			{
				String requete=this.fluxEntrant.readLine(); //Donnees obtenues aupres du client
				if(requete.isEmpty())
					continue;
				if(requete.equals("update")) {
					panel.getGraphes().clear();
					continue;
				}
					
				String[] donneesString = requete.split(","); // separer les donnees dans un tableau
				int length = donneesString.length;
				double[] donneesDouble = new double[length];
				int i = 0;
				
				// transformer les donnees de string a double
				for(String s:donneesString) {
					donneesDouble[i] = Double.parseDouble(s);
					i++;
				}
				
				// mis a jour la liste dans le panneau
				graphes.add(donneesDouble);
				panel.setGraphes(graphes);
				
				System.out.println("le client n"+this.noClient+" desire a designer "+requete);
				
			
			}
			
		} catch (SocketException e){
			System.out.println("session de dessin termine par le client");
	    }
		catch (NumberFormatException e)
	    {
			e.printStackTrace();
	    }
		catch (IOException e)
	    {
			e.printStackTrace();
	    }
	
	}




}
