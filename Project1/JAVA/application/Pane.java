package application;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import application.MyPanel;

/**
 * la Clasee pane c'est comme stage(anglais)
 */
@SuppressWarnings("serial")
public class Pane extends JFrame{
	private MyPanel panel;
	
	
	public static final String TITLE = "Graphe dessin";

    public static final int WIDTH = 800;
    public static final int HEIGHT = 800;

    public Pane() {
        super();
        initFrame();
    }
/**
 * c'est une methode pour initialiser frame
 */
    private void initFrame() {
        // Definition du titre et de la taille de la fenetre
        setTitle(TITLE);
        setSize(WIDTH, HEIGHT);

        // Definir l'action par defaut du bouton de fermeture de la fenetre (quitter le processus lorsque vous cliquez sur fermer)
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // Regler la position de la fenetre au centre de l'ecran
        setLocationRelativeTo(null);

        // mis en place du panneau de contenu de la fenetre
        panel = new MyPanel(this);
        setContentPane(panel);
    }
/**
 * c'est une methode de getter getPanel
 * @return  MyPanel panel
 */
	public MyPanel getPanel() {
		return panel;
	}
/**
 * c'est une methode de setter setPanel
 * @param panel MyPanel
 */
	public void setPanel(MyPanel panel) {
		this.panel = panel;
	}
    
    
}