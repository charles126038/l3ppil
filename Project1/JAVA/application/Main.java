package application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


import serveur.ReceveurEnvoyeur;

public class Main {

	
	public static void main(String[] args) {
		try {	
			int portServeur=9111;
			@SuppressWarnings("resource")
			ServerSocket serveur =new ServerSocket(portServeur);

			System.out.println("server demarre"+serveur);
			
			
			int noConnexion = 0;
			while(true) 
			{
				System.out.println("attente d'une nouvelle connexion");
				Socket interlocuteurClient =serveur.accept();
				System.out.println("connexion reussie n"+noConnexion);
				System.out.println("nouveau client connecte: "+interlocuteurClient);
				ReceveurEnvoyeur receveurEnvoyeur =new ReceveurEnvoyeur(interlocuteurClient,noConnexion);
				receveurEnvoyeur.start();// dans la methode start() il va lancer la methode run()(recharger)
				++noConnexion;
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}

	
}


