package application;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;

import cor_dessin.COR_Cercle;
import cor_dessin.COR_Polygone;
import cor_dessin.COR_Segment;
import cor_dessin.Dessin_COR;


/**
 * definie une planche a dessin comme screen(anlais)
 * frame ��planche
 * detecteur : la chaine de responsabilite
 * graphes : une liste des tableau de double qui contient des donnee des graphes (recu du serveur, la classe ReceveurEnvoyer)
 */
@SuppressWarnings("serial")
public class MyPanel extends JPanel {
	
 	private Pane frame;
 	private Dessin_COR detecteur;
 	private ArrayList<double[]> graphes;
 	private Timer timer; // minuteries
 	private int intervel = 1000 / 100; // intervalle de temps(ms)
 	/**
	 *C'est une methode de getter 
	 *@return Pane frame
	*/
 	public Pane getFrame() {
 		return frame;
 	}
	/**
	  *C'est une methode de setter
	  *@param frame ��planche
	 */
 	public void setFrame(Pane frame) {
 		this.frame = frame;
 	}
	/**
	  *C'est une methode de getter
	  *@return graphes qui est un vector qui contient des double[]
	 */ 	
 	public ArrayList<double[]> getGraphes() {
		return graphes;
	}
	/**
	 * c'est une methode de setter
	 * @param graphes : une liste des tableau de double qui contient des donnee des graphes (recu du serveur, la classe ReceveurEnvoyer)
	 */
	public void setGraphes(ArrayList<double[]> graphes) {
		this.graphes = graphes;
	}

 	/**
 	 * c'est une constructeur 
 	 * @param frame:planche
 	 */
 	public MyPanel(Pane frame) {
 		super();
 		this.frame = frame;
 		graphes = new ArrayList<double[]>();
	    construitChaineResponsabilite();
 	}
 	
 	@Override
 	protected void paintComponent(Graphics g) {
         super.paintComponent(g);
         // pour chaque forme, appeler la chaine de responsabilite
         if(!graphes.isEmpty()) {
	         for(double[] graphe:graphes) {
	        	 detecteur.detecter(graphe, g);
	         }
         }
 	}
 	
 	/**
 	 * cette methode est comme rendre(),Mise a jour en temps r��el de l'ecran de dessin
 	 */
 	public void run() {
 		timer = new Timer(); // Maitrise des processus
 		timer.schedule(new TimerTask() {
            @Override
            public void run() {
                repaint(); // Redessiner, en appelant la methode paintComponent()
            }
        }, intervel, intervel);
 	}

 	/**
 	 * c'est une methode pour construire ChaineResponsabilite
 	 */
 	private void construitChaineResponsabilite() {
		detecteur = new COR_Cercle();
		ajouterDetecteur(new COR_Segment());
		ajouterDetecteur(new COR_Polygone());
	}
	
	
	/**
	 * ajouter un detecteur dans la chaine de responsabilite 
	 * @param sui Dessin_COR
	 */
	private void ajouterDetecteur(Dessin_COR sui) {
		assert sui != null : "Le detecteur ne doit pas etre null !";
		// ajouter sui dans la tete de la liste
		sui.setSuivant(detecteur);
		detecteur = sui;
	}
 	
}
    
    
    
    
	
	
    

