package cor_dessin;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
/**
 * c'est une classe qui est une methode pour dessiner Segment dans  la chaine de responsablite 
 * 
 *
 */
public class COR_Segment extends Dessin_COR{

	@Override
	public boolean estDetectee(int GraphicalType, Graphics g) {
		if(GraphicalType == SEGMENT)
			return true;
		return false;
	}

	@Override
	public void designer(double[] grapheDonnee, Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();
		
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        setColor(g2d, (int)grapheDonnee[1]);

        // designer un segment
        g2d.drawLine((int)grapheDonnee[2], (int)grapheDonnee[3], (int)grapheDonnee[4], (int)grapheDonnee[5]);
        
        g2d.dispose();
	}

}
