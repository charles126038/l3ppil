package cor_dessin;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
/**
 * c'est une classe qui est une methode pour dessiner Polygone dans  la chaine de responsablite 
 * 
 *
 */
public class COR_Polygone extends Dessin_COR{

	@Override
	public boolean estDetectee(int GraphicalType, Graphics g) {
		if(GraphicalType == POLYGONE)
			return true;
		return false;
	}

	@Override
	public void designer(double[] grapheDonnee, Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();
		
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        setColor(g2d, (int)grapheDonnee[1]);

		// designer un Polygone
        int nPoints = (int)grapheDonnee[2];
        int i = 0;
        while(i/2 < nPoints-1) {
        	g2d.drawLine((int)grapheDonnee[i+3], (int)grapheDonnee[i+4], (int)grapheDonnee[i+5], (int)grapheDonnee[i+6]);
        	i = i + 2;
        }
        //Relier le dernier point au premier point
        g2d.drawLine((int)grapheDonnee[i+3], (int)grapheDonnee[i+4], (int)grapheDonnee[3], (int)grapheDonnee[4]);
        

        g2d.dispose();
	}

}
