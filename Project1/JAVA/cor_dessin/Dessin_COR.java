package cor_dessin;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Une classe de la chaine de responsablite qui dessine un graphe transforme du client
 */
public abstract class Dessin_COR {
	public static final int CERCLE = 1;
	public static final int SEGMENT = 2;
	public static final int POLYGONE = 3;
	
	private Dessin_COR _suivant;
	
	/**
	 * c'est une methode de getter de getSuivant
	 * @return Dessin_COR _suivant
	 */
	public Dessin_COR getSuivant() {
		return this._suivant;
	}
	/**
	 * c'est une methode de setter de setSuivant
	 * @param suivant mettre le suivant 
	 */
	public void setSuivant(Dessin_COR suivant) {
		this._suivant = suivant;
	}
	/**
	 * c'est une classe pour verifier le type de forme
	 * @param GraphicalType des types de forme
	 * @param g c'est une classe pour dessiner
	 * @return boolean
	 */
	public abstract boolean estDetectee(int GraphicalType, Graphics g);  // detecter le type du graphe
	
	/**
	 * c'est une class pour dessiner
	 * @param grapheDonnee : un tableau de double qui contient les donnees de la shape. 
	 * 						grapheDonnee[0] : type, 1 : couleur, suprerieur ou egale a 2 : coordonnees
	 * @param g : Graphics2D
	 */
	public abstract void designer(double[] grapheDonnee, Graphics g);  // action designer
	
	/**
	 *  c'est une classe de la chaine de responsabilite pour transmettre le suivant
	 * @param grapheDonnee : un tableau de double qui contient les donnees de la shape. 
	 * 						grapheDonnee[0] : type, 1 : couleur, suprerieur ou egale a 2 : coordonnees
	 * @param g : Graphics2D
	 */
	public void detecter(double[] grapheDonnee, Graphics g) {
		if(estDetectee((int)grapheDonnee[0], g)) {
			designer(grapheDonnee, g);
		}
		else if(_suivant != null) {
			_suivant.detecter(grapheDonnee, g);
		}
	}
	
	/**
	 * mis a jour la couleur du forme
	 * @param g : graphic designant
	 * @param color : black = 1, blue = 2, red = 3, green = 4, yellow = 5 et cyan = 6
	 */
	public void setColor(Graphics g, int color) {
		switch (color) {
		case 1:
			g.setColor(Color.black);
			break;
		case 2:
			g.setColor(Color.blue);
			break;
		case 3:
			g.setColor(Color.red);
			break;
		case 4:
			g.setColor(Color.green);
			break;
		case 5:
			g.setColor(Color.yellow);
			break;
		case 6:
			g.setColor(Color.cyan);
			break;
		default:
			break;
		}
	}
}
