#include <winsock2.h>
#include <iostream>
#include <sstream>
#include <string>
#include <string.h>
#include "Singleton.h"
#include "Client_Dessin.h"
#include "../Exception/Erreur.h"

using namespace std;
#pragma warning(disable:4996)
Client_Dessin::Client_Dessin(const string& adresseServeur, const int portServeur)
{
    Singleton::getInstance(); // initialisation de la DLL : effectuee une seule fois

    //---------------------- creation socket -------------------------------------------------

    int familleAdresses = AF_INET;         // IPv4
    int typeSocket = SOCK_STREAM;          // mode connecte TCP
    int protocole = IPPROTO_TCP;           // protocole. On peut aussi mettre 0 et la fct choisit le protocole en fct des 2 1ers parametres
                                           // pour les valeurs des parametres : cf. fct socket dans la doc sur winsock

    this->sock = socket(familleAdresses, typeSocket, protocole);

    if (this->sock == INVALID_SOCKET)
    {
        ostringstream oss;
        oss << "la creation du socket a echoue : code d'erreur = " << WSAGetLastError() << endl;	// pour les valeurs renvoyees par WSAGetLastError() : cf. doc ref winsock
        throw Erreur(oss.str().c_str());
    }

    cout << "socket cree" << endl;

    //------------------------------ creation du representant du serveur ----------------------------------------

    
    /*sin.sin_family = AF_INET;
    sin.sin_port = htons(portServeur);*/
    sockaddr.sin_family = AF_INET;
    sockaddr.sin_addr.s_addr = inet_addr(adresseServeur.c_str());   // inet_addr() convertit de l'ASCII en entier
    sockaddr.sin_port = htons(portServeur);                 //htons() assure que le port est bien inscrit dans le format du reseau (little-endian ou big-endian)
    //inet_pton(AF_INET, adresseServeur, (void*)&sin.sin_addr.S_un.S_addr);

    //-------------- connexion du client au serveur ---------------------------------------

    int r = connect(this->sock, (SOCKADDR*)&sockaddr, sizeof(sockaddr));     // renvoie une valeur non nulle en cas d'echec.
                                                                    // Le code d'erreur peut etre obtenu par un appel a WSAGetLastError()

    if (r == SOCKET_ERROR)
        throw Erreur("La connexion a echoue");

    cout << "connexion au serveur de majuscule reussie" << endl;

}


Client_Dessin::~Client_Dessin(){
    int r = shutdown(this->sock, SD_BOTH);	// on coupe la connexion pour l'envoi et la reception
    // renvoie une valeur non nulle en cas d'echec. Le code d'erreur peut etre obtenu par un appel e WSAGetLastError()

    if (r == SOCKET_ERROR)
        throw Erreur("la coupure de connexion a echoue");


    r = closesocket(this->sock); // renvoie une valeur non nulle en cas d'echec. Le code d'erreur peut etre obtenu par un appel e WSAGetLastError()
    if (r) throw Erreur("La fermeture du socket a echoue");

    cout << "arret normal du client" << endl;
}

void Client_Dessin::requeteEnvoi(string requete)
{
    requete = requete + "\r\n";
    int r = send(this->sock, requete.c_str(), (int)requete.length(), 0); // envoi de la requete au serveur

    if (r == SOCKET_ERROR)
        throw Erreur("echec de l'envoi de la requete d'ouverture de fenetre graphique");
    cout << "requete envoye:" << requete << endl;
}
