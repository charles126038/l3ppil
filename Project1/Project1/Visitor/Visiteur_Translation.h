#pragma once
#include "Visiteur.h"

/**
 * @brief un visiteur pour faire la translation
*/
class Visiteur_Translation : public Visiteur{
private:
	Vecteur_2D v;
public:
	Visiteur_Translation(const Vecteur_2D &t) :v(t.getX(), t.getY()) { }
	void visite(Composant*) const;
	void visite(Cercle*) const;
	void visite(Segment*) const;
	void visite(Polygone*) const;
};