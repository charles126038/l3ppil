#include "Visiteur_Rotation.h"
/**
 * @brief viste Rotation pour le forme Composant
 * @param c Composant*
*/
void Visiteur_Rotation::visite(Composant* c) const
{
	vector<Forme*> formes = c->getFormes();
	for (Forme* f : formes) {
		f->accepte(this);
	}
}
/**
 * @brief visite Rotation pour le forme Cercle: type,couleur,x,y,rayon
 * @param c Cercle*
*/
void Visiteur_Rotation::visite(Cercle* c) const
{
	double cose = cos(angle);
	double sine = sin(angle);
	double x2 = c->getCentre().getX() * cose - c->getCentre().getY() * sine + centre.getX();
	double y2 = c->getCentre().getX() * sine + c->getCentre().getY() * cose + centre.getY();
	c->setCentre(x2, y2);
}
/**
 * @brief visite Rotation pour le forme Segment: type,couleur,x1,y1,x2,y2
 * @param s Segment*
*/
void Visiteur_Rotation::visite(Segment* s) const
{
	double cose = cos(angle);
	double sine = sin(angle);
	double x2 = s->getPoint1().getX() * cose - s->getPoint1().getY() * sine + centre.getX();
	double y2 = s->getPoint1().getX() * sine + s->getPoint1().getY() * cose + centre.getY();
	s->setPoint1(x2, y2);
	x2 = s->getPoint2().getX() * cose - s->getPoint2().getY() * sine + centre.getX();
	y2 = s->getPoint2().getX() * sine + s->getPoint2().getY() * cose + centre.getY();
	s->setPoint2(x2, y2);
}
/**
 * @brief visite Rotation pour le forme Polygone: type,couleur,nombreSommets,x1,y1,x2,y2,...
 * @param Polygone*
*/
void Visiteur_Rotation::visite(Polygone* p) const
{
	double cose = cos(angle);
	double sine = sin(angle);
	vector<Vecteur_2D> sommets = p->getSommets();
	for (int i = 0; i < (int)sommets.size(); i++) {
		double x2 = sommets[i].getX() * cose - sommets[i].getY() * sine + centre.getX();
		double y2 = sommets[i].getX() * sine + sommets[i].getY() * cose + centre.getY();
		Vecteur_2D point = Vecteur_2D(x2, y2);
		p->setSommet(point, i);
	}
}
