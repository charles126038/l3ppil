#include "Singleton.h"

Singleton::Singleton() {
	int r;

	r = WSAStartup(MAKEWORD(2, 0), &wsaData);       // MAKEWORD(2,0) sert a indiquer la version de la librairie a utiliser : 1 pour winsock et 2 pour winsock2

	/* en cas de succee, wsaData a initialisee et l'appel a renvoye la valeur 0 */

	if (r)
		throw Erreur("L'initialisation a echouee");

	cout << "initialisation winsock effectuee" << endl;
};

Singleton::~Singleton() {
	WSACleanup();
}

Singleton* Singleton::getInstance()
{
	if (!instanceUnique)
		instanceUnique = new Singleton;

	return instanceUnique;
}

Singleton* Singleton::instanceUnique = NULL;