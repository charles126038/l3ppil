#include "Visiteur_Sauvegarder.h"
#include <fstream>
#include <iostream>
#include "../Exception/Erreur.h"
using namespace std;

void Visiteur_Sauvegarder::sauvegarder(string s) const
{
	ofstream outfile;

	outfile.open(chemin, ios::app);
	outfile << s;

	cout << endl << chemin << " est bien enregistre." << endl;
	outfile.close();
}

Visiteur_Sauvegarder::Visiteur_Sauvegarder()
{
	string nom;
	cout << "Le nom du fichier: ";
	cin >> nom;
	chemin = "../Document_dessin/" + nom; // initialiser le chemin
}
/**
 * @brief viste Sauvegarder pour le forme Composant
 * @param c Composant*
*/
void Visiteur_Sauvegarder::visite(Composant* c) const
{
	vector<Forme*> formes = c->getFormes();
	string requete = c->versString() + "\n";
	sauvegarder(requete);
}
/**
 * @brief visite Sauvegarder pour le forme Cercle: type,couleur,x,y,rayon
 * @param c Cercle*
*/
void Visiteur_Sauvegarder::visite(Cercle* c) const
{
	string requete = c->versString() + "\n";
	sauvegarder(requete);
}
/**
 * @brief visite Rotation pour le forme Segment: type,couleur,x1,y1,x2,y2
 * @param s Segment*
*/
void Visiteur_Sauvegarder::visite(Segment* s) const
{
	string requete = s->versString() + "\n";
	sauvegarder(requete);
}
/**
 * @brief visite Sauvegarder pour le forme Polygone: type,couleur,nombreSommets,x1,y1,x2,y2,...
 * @param Polygone*
*/
void Visiteur_Sauvegarder::visite(Polygone* p) const
{
	string requete = p->versString() + "\n";
	sauvegarder(requete);
}
