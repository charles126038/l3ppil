#include "Visiteur_Homothetie.h"
/**
 * @brief viste Homothetie pour le forme Composant
 * @param c Composant*
*/
void Visiteur_Homothetie::visite(Composant* c) const
{
	vector<Forme*> formes = c->getFormes();
	for (Forme* f : formes) {
		f->accepte(this);
	}

}
/**
 * @brief visite Homothetie pour le forme Cercle: type,couleur,x,y,rayon
 * @param c Cercle*
*/
void Visiteur_Homothetie::visite(Cercle* c) const
{
	Vecteur_2D centre = v + _rapport * (c->getCentre() - v);
	c->setCentre(centre.getX(), centre.getY());
}
/**
 * @brief visite Homothetie pour le forme Segment: type,couleur,x1,y1,x2,y2
 * @param s Segment*
*/
void Visiteur_Homothetie::visite(Segment* s) const
{
	Vecteur_2D p1 = v + _rapport * (s->getPoint1() - v);
	Vecteur_2D p2 = v + _rapport * (s->getPoint2() - v);
	s->setPoint1(p1.getX(), p1.getY());
	s->setPoint2(p2.getX(), p2.getY());
}
/**
 * @brief visite Homotheite pour le forme Polygone: type,couleur,nombreSommets,x1,y1,x2,y2,...
 * @param Polygone*
*/
void Visiteur_Homothetie::visite(Polygone* p) const
{
	vector<Vecteur_2D> sommets = p->getSommets();
	for (int i = 0; i < (int)sommets.size(); i++) {
		Vecteur_2D point = v + _rapport * (sommets[i] - v);
		p->setSommet(point, i);
	}
}
