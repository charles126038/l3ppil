#pragma once
#include <WinSock2.h>
#include <iostream>
#include "../Exception/Erreur.h"

/**
 * @brief initialisation de la DLL : effectuee une seule fois
*/
class Singleton
{
private:
	WSADATA wsaData;        // structure contenant les donnees de la librairie winsock a initialiser
	Singleton();
	~Singleton();
public:
	static Singleton* instanceUnique;
	static Singleton* getInstance();
};