#pragma once
#include "Client_Dessin.h"
#include "Visiteur.h"

/**
 * @brief un visteur pour dessiner des formes
*/
class Visiteur_Designer : public Visiteur{

	Client_Dessin* client;
public:
	Visiteur_Designer(Client_Dessin* c) :client(c) { }

	void visite(Composant*) const;
	void visite(Cercle*) const;
	void visite(Segment*) const;
	void visite(Polygone*) const;
};