#pragma once
#include "../Exception/Erreur.h"
#include "Visiteur.h"
#include <math.h>
#define PI 3.1415


/**
 * @breif un visiteur pour faire la rotaiton 
 * Une rotation est definie par un point invariant (le centre de la rotation) 
 * et par un angle signe donne en radian/degre (il va convertit le degre en radian automatiquement)
 * y2=x1*sin(angle)+y1*cos(angle);
 * x2=x1*cos(angle)-y1*sin(angle);
*/
class Visiteur_Rotation :public Visiteur {
private:
	Vecteur_2D centre;
	double angle;
	bool _isUseDegree;
public:
	Visiteur_Rotation(const Vecteur_2D& v, const double& radian, const bool &isUseDegree = true) :centre(v), _isUseDegree(isUseDegree) {
		if (fabs(angle) < 1e-6) throw Erreur("Erreur angle !");
		if (isUseDegree) {
			angle = radian / 180 * PI;//PI = 3.1415
		}
		else {
			angle = radian;
		}
	}
	void visite(Composant*) const;
	void visite(Cercle*) const;
	void visite(Segment*) const;
	void visite(Polygone*) const;
};
