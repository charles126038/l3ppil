#pragma once
#include<iostream>
#include<math.h>
#include"../Forme/Composant.h"
#include"../Forme/Segment.h"
#include"../Forme/Polygone.h"
#include"../Forme/Cercle.h"
using namespace std;

class Forme;
class Composant;
class Cercle;
class Segment;
class Polygone;
/**
 * @brief c'est une classe de base de visiteur
*/
class Visiteur {
public:
	Visiteur() {};
	virtual ~Visiteur() {};

	virtual void visite(Composant *)const = 0;
	virtual void visite(Cercle *)const = 0;
	virtual void visite(Segment *)const = 0;
	virtual void visite(Polygone *)const = 0;
};