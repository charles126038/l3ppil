#pragma once
#include "Visiteur.h"
#include <string>
/**
 * @brief un visiteur pour sauvegarder des information des formes dans le txt
*/
class Visiteur_Sauvegarder :public Visiteur {
private:
	string chemin;
	void sauvegarder(string s) const;
public:
	Visiteur_Sauvegarder();
	void visite(Composant* ) const;
	void visite(Cercle* ) const;
	void visite(Segment* ) const;
	void visite(Polygone* ) const;

};