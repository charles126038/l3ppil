#pragma once
#include <WinSock2.h>
#include <string>

using namespace std;

/**
 * @brief Client_Dessin.h
 *
 * Client vers le serveur de majuscule.
 *
 * Tache :
 * 1) demande a l'utilisateur, l'adresse IP et le port du serveur de majuscule // initialise a 127.0.0.1/9111
 * 2) se connecte au serveur
 * 3) utilisant un pattern de visiteur (a choisir)
 *
*/
class Client_Dessin {
private:
	SOCKET sock;  // informations concernant le socket a creer : famille d'adresses acceptees, mode connecte ou non, protocole
	SOCKADDR_IN sockaddr; // informations concernant le serveur avec lequel on va communiquer
public:
	Client_Dessin(const string& adresseServeur = "127.0.0.1", const int portServeur = 9111);
	~Client_Dessin();
	SOCKET getSocket() { return sock; }
    // envoyer le message vers serveur
	void requeteEnvoi(string requete);
};