#pragma once

#include "Visiteur.h"

/**
 *  @breif un visiteur pour faire la homothetie
 *  Une homothetie est definie par un point invariant et par un rapport d'homothetie (nombre reel quelconque).
 *	v2 = v + rapport * (v1 - v);
*/
class Visiteur_Homothetie : public Visiteur{
private:
	Vecteur_2D v;
	double _rapport;
public:
	Visiteur_Homothetie(const Vecteur_2D& t, const double &rapport) :v(t.getX(), t.getY()), _rapport(rapport) { }
	void visite(Composant*) const;
	void visite(Cercle*) const;
	void visite(Segment*) const;
	void visite(Polygone*) const;
};