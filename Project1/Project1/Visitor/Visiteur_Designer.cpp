#include "VIsiteur_Designer.h"

/**
 * @brief visite Dessigner pour le forme Composant
 * @param c Composant*
*/
void Visiteur_Designer::visite(Composant* c) const
{
	vector<Forme*> formes = c->getFormes();
	for (Forme* f : formes) {
		f->accepte(this);
	}
}


/**
 * @brief visite Dessigner pour le forme Cercle: type,couleur,x,y,rayon
 * @param c Cercle*
*/
void Visiteur_Designer::visite(Cercle* c) const
{
	client->requeteEnvoi(c->versString());
}



/**
 * @brief visite Dessigner pour le forme Segment: type,couleur,x1,y1,x2,y2
 * @param s Segment*
*/
void Visiteur_Designer::visite(Segment* s) const
{
	client->requeteEnvoi(s->versString());
}
 

/**
 * @brief visite Dessiner pour le forme Polygone: type,couleur,nombreSommets,x1,y1,x2,y2,...
 * @param Polygone*
*/
void Visiteur_Designer::visite(Polygone* p) const
{
	client->requeteEnvoi(p->versString());
}
