#include "Visiteur_Translation.h"
/**
 * @brief visite Translation pour le forme Composant
 * @param c Composant*
*/
void Visiteur_Translation::visite(Composant* c) const
{
	vector<Forme*> formes = c->getFormes();
	for (Forme* f : formes) {
		f->accepte(this);
	}
}
/**
 * @brief visite Translation pour le forme Cercle: type,couleur,x,y,rayon
 * @param c Cercle*
*/
void Visiteur_Translation::visite(Cercle* c) const
{
	Vecteur_2D centre = c->getCentre() + this->v;
	c->setCentre(centre.getX(), centre.getY());
}
/**
 * @brief visite Translation pour le forme Segment: type,couleur,x1,y1,x2,y2
 * @param s Segment*
*/
void Visiteur_Translation::visite(Segment* s) const
{
	Vecteur_2D point1 = s->getPoint1() + this->v;
	Vecteur_2D point2 = s->getPoint2() + this->v;
	s->setPoint1(point1.getX(), point1.getY());
	s->setPoint2(point2.getX(), point2.getY());
}
/**
 * @brief visite Translation pour le forme Polygone: type,couleur,nombreSommets,x1,y1,x2,y2,...
 * @param Polygone*
*/
void Visiteur_Translation::visite(Polygone* p) const
{
	vector<Vecteur_2D> sommets = p->getSommets();
	for (int i = 0; i < (int)sommets.size(); i++) {
		Vecteur_2D point = sommets[i] + v;
		p->setSommet(point, i);
	}
}
