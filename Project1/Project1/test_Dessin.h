#pragma once
#include "Visitor/VIsiteur_Designer.h"
#include "Visitor/Visiteur_Sauvegarder.h"
#include "Visitor/Visiteur_Translation.h"
#include "Visitor/Visiteur_Homothetie.h"
#include "Visitor/Visiteur_Rotation.h"

class test_Dessin {
public:
	Forme* designerForme() {
		Forme* f;
		cout << "Quelle forme vous voulez designer? " << endl;
		cout << "** cercle = 1, segment = 2, polynome = 3, composant = 4 **" << endl;
		int type;
		do {
			cin >> type;
		} while (type < 1 || type > 4);

		cout << "Couleur ?" << endl;
		cout << "** black = 1, blue = 2, red = 3, green = 4, yellow = 5 et cyan = 6 **" << endl;
		int couleur;
		do {
			cin >> couleur;
		} while (couleur < 1 || couleur > 6);


		switch (type)
		{
			// cercle
		case 1: {
			cout << "Saisir x de la centre(x,y): ";
			double x; cin >> x;
			cout << endl << "Saisir y de la centre(x,y): ";
			double y; cin >> y;
			cout << endl << "Saisir le rayon: ";
			double rayon; cin >> rayon;
			Cercle* forme = new Cercle(x, y, rayon);
			forme->setCouleur(couleur);
			f = forme;
			break;
		}
			  // segment
		case 2: {
			cout << "Saisir x1 du point1(x1,y1): ";
			double x1; cin >> x1;
			cout << endl << "Saisir y1 du point1(x1,y1): ";
			double y1; cin >> y1;
			cout << endl << "Saisir x2 du point2(x2,y2): ";
			double x2; cin >> x2;
			cout << endl << "Saisir y2 du point2(x2,y2): ";
			double y2; cin >> y2;
			Segment* forme = new Segment(x1, y1, x2, y2);
			forme->setCouleur(couleur);
			f = forme;
			break;
		}
			  // polygone
		case 3: {
			cout << "Saisir le nombre des points du polygone: ";
			int nb; cin >> nb;
			vector<double> sommets;
			for (int i = 0; i < nb; i++) {
				cout << endl << "Saisir x" << i + 1 << " du point" << i + i << "(x" << i + 1 << ",y" << i + 1 << "): ";
				double x; cin >> x;
				cout << endl << "Saisir y" << i + 1 << " du point" << i + i << "(x" << i + 1 << ",y" << i + 1 << "): ";
				double y; cin >> y;
				sommets.push_back(x);
				sommets.push_back(y);
			}
			Polygone* forme = new Polygone(nb, sommets);
			forme->setCouleur(couleur);
			f = forme;
			break;
		}
			  // Composant
		case 4: {
			cout << "Construire un composant: " << endl;
			Composant* forme = new Composant(couleur);
			forme->add(designerForme());
			f = forme;
			break;
		}
		default:
			return NULL;
			break;
		}
		return f;
	}

	void Action(Forme* f, int type) {
		switch (type)
		{
			// translation
		case 1: {
			cout << "Donnez x de vector(x,y) que vous voulez transler: ";
			double x; cin >> x;
			cout << endl << "Donnez y de vector(x,y) que vous voulez transler: ";
			double y; cin >> y;
			Visiteur_Translation* translation = new Visiteur_Translation(Vecteur_2D(x, y));
			f->accepte(translation);
			break;
		}
			  // rotation
		case 2: {
			cout << "Donnez x de centre(x,y) que vous voulez tourner: ";
			double x; cin >> x;
			cout << endl << "Donnez y de centre(x,y) que vous voulez tourner: ";
			double y; cin >> y;
			cout << endl << "Donnez radian ou degree que vous voulez tourner: ";
			double radian; cin >> radian;
			Visiteur_Rotation* rotation = new Visiteur_Rotation(Vecteur_2D(x, y), radian);
			f->accepte(rotation);
			break;
		}
			  // homothetie
		case 3: {
			cout << "Donnez x de centre(x,y) que vous voulez faire homotheite: ";
			double x; cin >> x;
			cout << endl << "Donnez y de centre(x,y) que vous voulez faire homothetie: ";
			double y; cin >> y;
			cout << endl << "Donnez rapport/distance que vous voulez faire homothetie: ";
			double rapport; cin >> rapport;
			Visiteur_Homothetie* homo = new Visiteur_Homothetie(Vecteur_2D(x, y), rapport);
			f->accepte(homo);
			break;
		}

		default:
			break;
		}
	}
};