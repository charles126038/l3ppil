#include "COR_Dessin/COR_Forme_Composant.h"
#include "Visitor/Client_Dessin.h"
#include "Visitor/VIsiteur_Designer.h"
#include "Visitor/Visiteur_Sauvegarder.h"
#include "Visitor/Visiteur_Translation.h"
#include "Visitor/Visiteur_Homothetie.h"
#include "Visitor/Visiteur_Rotation.h"
#include "test_Dessin.h"

#include <string.h>
#pragma comment(lib, "ws2_32.lib") // specifique a VISUAL C++


int main() {
	try {
		// ----- test -----
		/*cout << "essai des vecteurs 2D \n";
		Vecteur_2D u1 = Vecteur_2D("(2,3)");
		Vecteur_2D u2(20, 80), v(5), w, v1(35, -63), u3(3, 4), u4(3, -4), v3;

		cout << " u1 = " << u1 << endl;
		cout << " u2 = " << u2 << endl;
		cout << " u1 - u2 = " << u1 - u2 << endl;
		cout << " 5*u1 = " << 5 * u1 << endl;

		cout << endl;
		cout << "essai des cercles \n";
		Cercle* c1 = new Cercle(3, 4, 6);
		Cercle* c2 = new Cercle(u2, 5);
		Cercle* c3 = new Cercle(*c1);
		cout << " c1 = " << *c1 << endl;
		cout << " c2 = " << *c2 << endl;
		cout << " c3 = " << *c3 << endl;

		cout << endl;
		cout << "essai des segment \n";
		Segment* s1 = new Segment(3, 4, 8, 9);
		Segment* s2 = new Segment(u1, u2);
		Segment* s3 = new Segment(*s1);
		cout << " s1 = " << *s1 << endl;
		cout << " s2 = " << *s2 << endl;
		cout << " s3 = " << *s3 << endl;

		cout << endl;
		cout << "essai des Polygone \n";
		int nombrePoints;
		vector<double> points;
		cout << "nombre de points: ";
		cin >> nombrePoints;
		for (int i = 0; i < nombrePoints; i++) {
			cout << "Saisir le abscisse(axe X) du " << i + 1 << "eme sommet:";
			double x, y;
			cin >> x;
			points.push_back(x);
			cout << "Saisir l'axe Y du " << i + 1 << "eme sommet:";
			cin >> y;
			points.push_back(y);
		}
		Polygone* p1 = new Polygone(nombrePoints, points);
		Polygone* p2 = new Polygone(*p1);
		cout << " p1 = " << *p1 << endl;
		cout << " p2 = " << *p2 << endl;

		Composant* cc1 = new Composant();
		(*cc1).add(c1);
		(*cc1).add(s1);
		(*cc1).add(p1);

		cout << (*cc1) << endl;

		//cout << endl;
		//cout << "essai de lire fichier \n";

		////construire une chaine de responsabilite
		//COR_Forme_Cercle* corC = new COR_Forme_Cercle();
		//COR_Forme_Polygone* corP = new COR_Forme_Polygone(corC);
		//COR_Forme_Segment* corS = new COR_Forme_Segment(corP);
		//COR_Forme_Composant* corCo = new COR_Forme_Composant(corS);
		//char adr[10] = "text.txt";
		//ChargerFichier* charger = new ChargerFichier(adr, corCo);
		//vector<Forme*> formes = charger->Charger();
		//cout << "nombre de formes: " << formes.size() << endl;
		//
		//Client_Dessin* client = new Client_Dessin();

		//cout << endl;
		//cout << "essai de envoyer requete \n";
		//Visiteur_Designer* designer = new Visiteur_Designer(client);
		//for (Forme* f : formes) {
		//	cout << *f << endl;
		//	f->accepte(designer);
		//}

		//cout << endl;
		//cout << "--------------essai de sauvegarder dans le fichier-----------------\n";
		//Visiteur_Sauvegarder* sauvegarder = new Visiteur_Sauvegarder();
		//for (Forme* f : formes) {
		//	cout << *f << endl;
		//	f->accepte(sauvegarder);
		//}

		cout << endl;
		cout << "-------------essai de translation------------ \n";
		Visiteur_Translation* translation = new Visiteur_Translation(u2);
		for (Forme* f : formes) {
			cout << *f << endl;
			f->accepte(translation);
			cout << "*** apres translation: **** \n";
			cout << *f << endl;
			f->accepte(designer);
		}

		cout << endl;
		cout << "-------------essai de homothetie-------------- \n";
		Visiteur_Homothetie* homothetie = new Visiteur_Homothetie(u2, 6);
		for (Forme* f : formes) {
			cout << *f << endl;
			f->accepte(homothetie);
			cout << "*** apres homothetie: **** \n";
			cout << *f << endl;
			f->accepte(designer);
		}

		cout << endl;
		cout << "-------------essai de rotation-------------- \n";
		Visiteur_Rotation* rotation = new Visiteur_Rotation(u2, 90);
		client->requeteEnvoi("update");
		for (Forme* f : formes) {
			cout << *f << endl;
			f->accepte(rotation);
			cout << "*** apres rotation: **** \n";
			cout << *f << endl;
			f->accepte(designer);
		}*/
		// ----------------
		
		// commencer --
		string adresseServeur;
		short portServeur;

		cout << "tapez l'adresse IP du serveur de majuscule : " << endl;
		cin >> adresseServeur;
		cout << "tapez le port du serveur du serveur de majuscule : " << endl;

		cin >> portServeur;

		Client_Dessin* client = new Client_Dessin(adresseServeur, portServeur); // creer le socket
		Visiteur_Designer* designer = new Visiteur_Designer(client);
		vector<Forme*> formes; // la liste contient les forme
		test_Dessin test = test_Dessin();

		bool continuer = true;
		cout << "\n\t ***** Dessiner formes *****  Quitter tapez: quit" << endl;

		cout << "Voulez-vous importer un fichier? O ou N: ";
		string reponse; cin >> reponse;
		if (reponse == "quit") {
			continuer = false;
		}
		else {
			while (reponse != "O" && reponse != "N") {
				cout << "Veuillez saisir O ou N" << endl;
				cin >> reponse;
			} 

			// --------- importer le fichier ------------
			if (reponse == "O") {
				cout << "Veuillez saisir le chemin du fichier: " << endl;
				string chemin; cin >> chemin;
				if (chemin == "quit") {
					return 0;
				}
				//construire une chaine de responsabilite
				COR_Forme_Cercle* corC = new COR_Forme_Cercle();
				COR_Forme_Polygone* corP = new COR_Forme_Polygone(corC);
				COR_Forme_Segment* corS = new COR_Forme_Segment(corP);
				COR_Forme_Composant* corCo = new COR_Forme_Composant(corS);

				ChargerFichier* chargeur = new ChargerFichier(chemin, corCo);
				formes = chargeur->Charger();
				for (Forme* f : formes) {
					f->accepte(designer);
				}
				cout << "Voulez-vous encore designer? O ou N: ";
				cin >> reponse;
				while (reponse != "O" && reponse != "N" && reponse != "quit") {
					cout << "Veuillez saisir O ou N" << endl;
					cin >> reponse;
				}
				if (reponse == "quit" || reponse == "N") {
					continuer = false;
				}
				else {
					// -----  designer forme  -----
					if (reponse == "O") {
						Forme* f = test.designerForme();
						formes.push_back(f);
						f->accepte(designer);
					}
				}

			}
			else {
				bool con = true;
				do {
					cout << "Voulez-vous encore designer? O ou N: ";
					cin >> reponse;
					while (reponse != "O" && reponse != "N" && reponse != "quit") {
						cout << "Veuillez saisir O ou N" << endl;
						cin >> reponse;
					}
					if (reponse == "quit") {
						continuer = false;
					}
					else {
						// -----  designer forme  -----
						if (reponse == "O") {
							Forme* f = test.designerForme();
							formes.push_back(f);
							f->accepte(designer);
						}
						else
							con = false;
					}
				} while (con);
			}
			
		}


		if (continuer) {
			do {
				cout << "\nVoulez-vous faire des actions? O ou N: ";
				cin >> reponse;
				while (reponse != "O" && reponse != "N") {
					cout << "Veuillez saisir O ou N: ";
					cin >> reponse;
				}
				if (reponse == "O") {
					for (Forme* f : formes) {
						cout << *f << endl;
						cout << "Pour ce forme, voulez-vous faire des actions? O ou N: ";
						string reponse; cin >> reponse;
						while (reponse != "O" && reponse != "N") {
							cout << "Veuillez saisir O ou N: ";
							cin >> reponse;
						}
						if (reponse == "O") {
							cout << "Quelle action voulez-vous faire? ";
							cout << "\n *** Translation = 1, Rotation = 2, Homothetie = 3 ****" << endl;
							int action;
							do {
								cin >> action;
							} while (action < 1 || action > 3);
							// action 
							test.Action(f, action);
							client->requeteEnvoi("update");
							for (Forme* f : formes)
								f->accepte(designer);
						}
					}

				}
			} while (reponse == "O");
		}
		
		
		cout << "Voulez-vous sauvegarder ? O ou N: ";
		cin >> reponse;
		while (reponse != "O" && reponse != "N") {
			cout << "Veuillez saisir O ou N" << endl;
			cin >> reponse;
		}
		if (reponse == "O") {
			Visiteur_Sauvegarder* sauvegarder = new Visiteur_Sauvegarder();
			for (Forme* f : formes) {
				f->accepte(sauvegarder);
			}
		}
		
	}
	catch (Erreur e) {
		e.erreur();
	}
	
	system("pause");
}
