#pragma once

#include <exception>
#include <stdio.h>
#include <iostream>

using namespace std;
/**
 * @brief Une humble erreur, pour notre propre usage
*/
class Erreur :public exception {
private:
	char* _s;

public:
	Erreur(const char* s) {
		_s = _strdup(s);
	}

	void erreur() {
		cout << this->_s << endl;
		//Sleep(3000);
		exit(1);
	}
};
