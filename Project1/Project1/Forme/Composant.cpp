#include "Composant.h"



void Composant::setCouleur(int couleur)
{
	for (int i = 0; i < (int)Composant_Formes.size(); i++)
		Composant_Formes[i]->setCouleur(couleur);
}

string Composant::versString() const
{
	vector<Forme*> formes = getFormes();
	string s = to_string(Forme::COMPOSANT) + "," + to_string(getNombreForme()) + ";";
	for (Forme* f : formes) {
		s = s + f->versString() + ";";
	}
	s.erase(s.end() - 1);
	return s;
}


ostream& operator << (ostream& os, const Composant& c) {


	os << string(c);
	return os;


}


 double Composant::aire() const
{
	double s = 0.0;
	for (int i = 0; i < (int)Composant_Formes.size(); i++)
		s += Composant_Formes[i]->aire();
	return s;
}

 void Composant::accepte(const Visiteur* visiteur)
 {
	 visiteur->visite(this);
 }
