#pragma once
#include <iostream>
#include "../Exception/Erreur.h"
#include "Vecteur_2D.h"
#include <string>

using namespace std;
class Visiteur;

/**
 * @breif C'est une classe de base 
 * Cette classe contient 3 variables : 
 * - forme(int) : cercle = 1, segment = 2, polynome = 3, composant = 4
 * - couleur(int) : black = 1, blue = 2, red = 3, green = 4, yellow = 5 et cyan = 6
 * - est_ajoute(bool) : la forme est ajoutee dans une composant ou pas
*/
class Forme {
private:
	int _forme;
	int _couleur;
	bool est_ajoute = false;

public:
	// --------------- couleur ----------------
	static const int BLACK = 1;
	static const int BLUE = 2;
	static const int RED = 3;
	static const int GREEN = 4;
	static const int YELLOW = 5;
	static const int CYAN = 6;

	// --------------- forme ------------------
	static const int CERCLE = 1;
	static const int SEGMENT = 2;
	static const int POLYGONE = 3;
	static const int COMPOSANT = 4;

	/**
	 * @brief un constructeur de la classe Forme
	 * @param forme int 
	 * @param color int
	 * @return 
	*/
	inline Forme(int forme, int color = BLACK) :_forme(forme), _couleur(color) {}
	/**
	 * @brief un constructeur de la classe Forme
	 * @param f const Forme
	 * @return 
	*/
	inline Forme(const Forme& f):_forme(f.getForme()), _couleur(f.getCouleur()), est_ajoute(f.getEstAjoute()){}
	
	/**
	 * @brief setter setCouleur
	 * @param color const int 
	*/
	inline void setCouleur(const int& color) { _couleur = color; }
	/**
	 * @brief setter estAjoute
	*/
	inline void estAjoute() { est_ajoute = true; } // ajouter dans une conposant
	/**
	 * @brief setter estRetire
	*/
	inline void estRetire() { est_ajoute = false; }
	/**
	 * @brief getter getCouleur
	 * @return const int
	*/
	inline const int getCouleur() const { return _couleur; }
	/**
	 * @brief getter getForme
	 * @return const int
	*/
	inline const int getForme() const { return _forme; }
	/**
	 * @brief getter getEstAjoute
	 * @return const bool
	*/
	inline const bool getEstAjoute() const { return est_ajoute; }
	/**
	 * @brief transformer la forme vers format string
	 * @return 
	*/
	virtual string versString() const = 0; 

	virtual void accepte(const Visiteur* visiteur) = 0;
	virtual  double aire()const = 0;
	virtual operator string() const = 0;
	friend ostream& operator << (ostream& os, const Forme& F);
	

	
};

