#include"Cercle.h"

ostream& operator << (ostream& os, const Cercle& c) {

	os << string(c);
	return os;

}


string Cercle::versString() const
{
	return to_string(Forme::CERCLE) + "," + to_string(getCouleur()) + "," + to_string(getCentre().getX()) + "," + to_string(getCentre().getY()) + "," + to_string(getRayon());

}

void Cercle::accepte(const Visiteur* visiteur)
{
	visiteur->visite(this);
}

double Cercle::aire()const  {


	return this->getRayon() * this->getRayon() * 3.14;


}