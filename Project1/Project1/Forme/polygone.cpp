#include "Polygone.h"

ostream& operator << (ostream& os, const Polygone& p) {

	os << string(p);
	return os;

}

string Polygone::versString() const
{
	string requete = to_string(Forme::POLYGONE) + "," + to_string(getCouleur()) + "," + to_string(getNombreSommets());
	vector<Vecteur_2D> sommets = getSommets();
	for (Vecteur_2D v : sommets) {
		requete = requete + "," + to_string(v.getX()) + "," + to_string(v.getY());
	}
	return requete;
}

void Polygone::accepte(const Visiteur* visiteur)
{
	visiteur->visite(this);
}

double Polygone::aire() const
{
	double a, b, c;
	double result = 0;
	double p;
	for (int i = 2; i < (int)_sommets.size(); i++)
	{
		a = sqrt(pow(_sommets[i - 1].getX() - _sommets[0].getX(), 2.0) + pow(_sommets[i - 1].getY() - _sommets[0].getY(), 2.0));
		b = sqrt(pow(_sommets[i].getX() - _sommets[i - 1].getX(), 2.0) + pow(_sommets[i].getY() - _sommets[i - 1].getY(), 2.0));
		c = sqrt(pow(_sommets[i].getX() - _sommets[0].getX(), 2.0) + pow(_sommets[i].getY() - _sommets[0].getY(), 2.0));
		p = (a + b + c) / 2;
		result += sqrt(p * (p - a) * (p - b) * (p - c));
	}
	return result;
}