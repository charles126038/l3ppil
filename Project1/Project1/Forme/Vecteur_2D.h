#pragma once
#include <string>
#include <iostream>
#include <sstream>
#include "../Exception/Erreur.h"

using namespace std;

template <class T>
inline const T operator - (const T& u, const T& v)
{
	return u + -v;
}

/**
 * @brief Vecteur_2D contient 2 doubles qui construisent la coordonnee de la forme 
 * -double x
 * -double y
*/
class Vecteur_2D {
private:
    double x, y;
public:
    /**
     * @brief un constructeur de la classe Vecteur_2D
     * @param x const double 
     * @param y const double
     * @return 
    */
    inline explicit Vecteur_2D(const double& x = 0, const double& y = 0);

    /**
     * DONNEES : s respectant le format "(nombre reel,nombre reel)"
     *
     * */
    inline Vecteur_2D(const char* s);

    inline  double getX() const { return x; }
    inline  double getY() const { return y; }

    inline void setX(const double& a) { x = a; }
    inline void setY(const double& b) { y = b; }
   
    inline const Vecteur_2D operator + (const Vecteur_2D& u) const;
    inline const Vecteur_2D operator * (const double& a) const;
    /**
     * - unaire (c'est-��- dire oppose d'un vecteur)
     * */
    inline const Vecteur_2D operator - () const;

    operator string() const;

    

}; 

inline const Vecteur_2D operator *(const double& a, const Vecteur_2D& u) { return u * a; }

//------------ implementation des fonctions inline ----------------------

inline  Vecteur_2D::
Vecteur_2D(const double& x, const double& y) : x(x), y(y) {}

/**
 * @brief un constructeur de la classe Vecteur_2D pour analyse le point en forme const char*
 * @param s const char*
 * @return 
*/
inline Vecteur_2D::Vecteur_2D(const char* s) {
    // tester 
    string c(s);
    string::size_type idx = c.find(",");
    if (idx == string::npos || c.at(0) != '(' || c.at(sizeof(s)) != ')')
        throw Erreur("Format non conform");

    // split (double, double) par ','
    char* s1;
    s1 = _strdup(s);
    char* cords;
    char* pTmp = NULL;
    cords = strtok_s(s1, ",", &pTmp);

    string _x(cords);
    _x.erase(0, 1); // supprimer '('
    x = stod(_x);
    cords = strtok_s(NULL, ",", &pTmp);
    string _y(cords);
    _y.erase(size(_y)-1, size(_y)); // supprimer ')'
    y = stod(_y);
    
}

inline const Vecteur_2D Vecteur_2D::operator + (const Vecteur_2D& u) const
{
    return Vecteur_2D(x + u.x, y + u.y);
}

inline const Vecteur_2D Vecteur_2D::operator * (const double& a) const
{
    return Vecteur_2D(x * a, y * a);
}

inline const Vecteur_2D Vecteur_2D::operator - () const
{
    return Vecteur_2D(-x, -y);
}

inline Vecteur_2D::operator string() const
{
    ostringstream os;
    os << "( " << x << ", " << y << " )";
    return os.str();
}


inline ostream& operator << (ostream& os, const Vecteur_2D& u)
{
    os << (string)u;
    return os;
}
