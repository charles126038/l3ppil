#pragma once
#include "Forme.h"
#include <vector>
#include "Cercle.h"
#include "Polygone.h"
#include "Segment.h"
#include "../Visitor/Visiteur.h"


using namespace std;
/**
 * @brief  une classe Composant qui herite de la classe Forme
 *-vector<Forme*>  Composant_Formes
*/
class Composant : public Forme
{
private:
	
	vector<Forme*> Composant_Formes;

public:
	/**
	 * @brief un constructeur de la classe Composant
	 * @param int Couleur 
	 * @return 
	*/
	Composant(int Couleur = 1) :Forme(COMPOSANT, Couleur) {}
	~Composant() {};
	
	/**
	 * @brief  ajoute une forme dans cette compose
	 * @param Forme *f 
	*/
	void add(Forme *f)
	{
		Composant_Formes.push_back(f);
		f->estAjoute();
	}
	/**
	 * @brief setter setCouleur
	 * @param couleur int
	*/
	void setCouleur(int couleur);
	/**
	 * @brief getterNombreForme
	 * @return int
	*/
	const int getNombreForme() const { return Composant_Formes.size(); }
	/**
	 * @brief getter getForme
	 * @return vector<Forme*>
	*/
	const vector<Forme*> getFormes() const { return Composant_Formes; }
	
	string versString() const;

	friend ostream& operator << (ostream& os, const Composant& c);
	operator string() const;
	/**
	 * @brief Surface calculee
	 * @return double
	*/
	virtual double aire()const;
	void accepte(const Visiteur* visiteur);

};





inline Composant::operator string() const {

	ostringstream os;
	os << "Composant [ \n";
	for (int i = 0; i < (int)Composant_Formes.size(); i++) {
		os << string(*Composant_Formes[i]) << endl;
	}
	os << " ]" << endl;
	return os.str();


}