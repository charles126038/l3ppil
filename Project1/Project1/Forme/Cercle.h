#pragma once

#include "Forme.h"
#include "../Visitor/Visiteur.h"
#include <iostream>

using namespace std;

/**
 * @brief une classe Cercle qui herite de la classe Forme
 * -Vecteur_2D _centre;
 * -double _rayon;
*/
class Cercle :public Forme {
private:
	Vecteur_2D _centre;
	double _rayon;

public:
	/**
	 * @brief un constructeur de la classe Cercle
	 * @param x const double
	 * @param y const double 
	 * @param rayon const double
	 * @return 
	*/
	inline explicit Cercle(const double &x, const double &y, const double &rayon) : Forme(CERCLE) {
		if (rayon < 0) throw Erreur("rayon ne doit pas etre inferieur a 0");
		_centre = Vecteur_2D(x, y);
		_rayon = rayon;
	}
	/**
	 * @brief un constructeur de la classe Cercle
	 * @param centre const Vecteur_2D
	 * @param rayon const double
	 * @return 
	*/
	inline explicit Cercle(const Vecteur_2D &centre, const double &rayon) :Forme(CERCLE),
		_centre(centre) {
		if (rayon < 0) throw Erreur("rayon ne doit pas etre inferieur a 0");
		_rayon = rayon;
	}

	/**
	 * @brief un constructeur de la classe Cercle
	 * @param c const Cercle
	 * @return 
	*/
	inline explicit Cercle(const Cercle &c): Forme(c),
		_centre(c._centre), _rayon(c._rayon) {}
	/**
	 * @brief getter getCentre
	 * @return Vecteur_2D _centre
	*/
	inline  Vecteur_2D getCentre() const { return _centre; }
	/**
	 * @brief getter getRayon
	 * @return double _rayon
	*/
	inline  double getRayon() const { return _rayon; }

	/**
	 * @brief setter setCentre
	 * @param x const double
	 * @param y const double
	*/
	inline void setCentre(const double &x, const double &y) { _centre = Vecteur_2D(x, y); }
	/**
	 * @brief setter setRayon
	 * @param rayon const double
	*/
	inline void setRayon(const double &rayon) { _rayon = rayon; }
	
	string versString() const;

	void accepte(const Visiteur* visiteur);
	
	inline const Cercle* clone() const { return new Cercle(*this); }
	/**
	 * @brief Surface calculee
	 * @return double
	*/
	virtual double aire()const;
	
	operator string() const;
	
	friend ostream& operator << (ostream& os, const Cercle& c);
};


inline Cercle::operator string() const {

	ostringstream os;
	os << "Cercle[ centre: " <<_centre << ", couleur: " << getCouleur() << ", rayon: " << _rayon << ", aire: " << aire() << " ]";
	return os.str();


}