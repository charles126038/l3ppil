#include "Segment.h"


ostream& operator << (ostream& os, const Segment& s) {

	os << string(s);
	return os;

}

string Segment::versString() const
{
	return to_string(Forme::SEGMENT) + "," + to_string(getCouleur()) + "," + to_string(getPoint1().getX()) + "," + to_string(getPoint1().getY()) + "," + to_string(getPoint2().getX()) + "," + to_string(getPoint2().getY());
}

void Segment::accepte(const Visiteur* visiteur)
{
	visiteur->visite(this);
}

double  Segment::aire()const  {

	return 0.0;

}