#pragma once
#include "Forme.h"
#include "../Visitor/Visiteur.h"

using namespace std;

/**
 * @brief une classe Segement qui herite de la classe Forme
 * Vecteur_2D _v1
 * Vecteur_2D _v2
*/
class Segment :public Forme {
private:
	Vecteur_2D _v1;
	Vecteur_2D _v2;

public:
	/**
	 * @brief un constructeur de la classe Segment
	 * @param x1 const double le point
	 * @param y1 const double le point
	 * @param x2 const double le point
	 * @param y2 const double le point
	 * @return 
	*/
	inline explicit Segment(const double &x1, const double &y1, const double &x2, const double &y2) :Forme(SEGMENT) {
		_v1 = Vecteur_2D(x1, y1);
		_v2 = Vecteur_2D(x2, y2);
	}
	/**
	 * @brief un constructeur de la classe Segment
	 * @param v1 const Vecteur_2D
	 * @param v2 cont Vecteur_2D
	 * @return 
	*/
	inline explicit Segment(const Vecteur_2D &v1, const Vecteur_2D &v2):Forme(SEGMENT),
		_v1(v1), _v2(v2) {}
	/**
	 * @brief un constructeur de la classe Segment
	 * @param s const Segment
	 * @return 
	*/
	inline explicit Segment(const Segment &s):Forme(s),
		_v1(s.getPoint1()), _v2(s.getPoint2()) {}

	/**
	 * @brief getter getPoint1
	 * @return const Vecteur_2D
	*/
	inline const Vecteur_2D getPoint1() const { return _v1; }
	/**
	 * @brief getter getPoint2
	 * @return const Vecteur_2D
	*/
	inline const Vecteur_2D getPoint2() const { return _v2; }
	/**
	 * @brief setter setPoint1
	 * @param x1 const double 
	 * @param y1 const double
	*/
	inline void setPoint1(const double &x1, const double &y1) { _v1 = Vecteur_2D(x1, y1); }
	/**
	 * @brief setter setPoint2
	 * @param x2 const double
	 * @param y2 const double
	*/
	inline void setPoint2(const double &x2, const double &y2) { _v2 = Vecteur_2D(x2, y2); }
	
	string versString() const;

	void accepte(const Visiteur* visiteur);

	inline const Segment* clone() const { return new Segment(*this); }
	/**
	 * @brief Surface calculee
	 * @return double
	*/
	virtual double aire()const;
	
	operator string() const;

	friend ostream& operator << (ostream& os, const Segment& s);


//protected:
//	ostream& afficher(ostream& os) const {
//		os << "Segment[  point1 = " << _v1 << ", point2 = " << _v2 << " ]" << endl;
//		return os;
//	}
};

inline Segment::operator string() const{

	ostringstream os;
	os << "Segment [ " << "couleur: " << getCouleur() << ", " << this->_v1 << ", " << this->_v2 << " ]";
	return os.str();


}