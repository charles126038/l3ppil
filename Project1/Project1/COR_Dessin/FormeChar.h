#pragma once
#include "../Forme/Forme.h"
#include <vector>
/**
 * @brief comme Expert
*/
class FormeChar
{
public:
	FormeChar(){}
	virtual ~FormeChar() {}

	virtual Forme* detecter(string ch) const = 0;
	

};

 