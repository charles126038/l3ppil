#pragma once
#include "FormeChar.h"
#include <fstream>
#include <sstream>
#include <string>
#include <vector>


/** 
 * @brief C'est une classe Transformer le fichier a des formes utilisant la chaine de responsabilite
 *  les formes dans un composant sont separees par ';'
 *  - adress : l'adresse du fichier
 *  - liste : la chaine de responsabilit
*/
class ChargerFichier {
private:
	string adress;
	FormeChar* liste;

public:
	/**
	 * @brief c'est un constructeur de la classe ChargerFichier
	 * @param adr l'adresse du fichier
	 * @param l FormeChar *
	 * @return 
	*/
	ChargerFichier(string adr, FormeChar* l) :adress(adr), liste(l){}

/**
 * @brief Charger:transformer le fichier a des formes utilisant la chaine de responsabilite
 * @return vector<Forme*> formes 
*/
	const vector<Forme*> Charger() {
		ifstream infile;
		infile.open(adress);
		vector<Forme*> formes;

		// Parcourir l'ensemble du fichier
		while (!infile.eof()) {
			string s;
			getline(infile, s, '\n'); // obtenir les proprietes de la forme par ligne
			if (Forme* f = liste->detecter(s)) {
				formes.push_back(f);
			}
		}
		infile.close();
		return formes;
	}
};

/**
 * @brief split : separer string dans un vector concernant une chaine de caractere
 * @param s le string s a traiter  
 * @param tokens  le parametre de la classe vector qui stocke des parametres string
 * @param delimiters  separateur
*/
void split(const string& s, vector<string>& tokens, const string& delimiters = " ") {
	string::size_type lastPos = s.find_first_not_of(delimiters, 0); 
	string::size_type pos = s.find_first_of(delimiters, lastPos); 
	while (string::npos != pos || string::npos != lastPos) {
		tokens.push_back(s.substr(lastPos, pos - lastPos));
		lastPos = s.find_first_not_of(delimiters, pos); 
		pos = s.find_first_of(delimiters, lastPos); 
	} 
}