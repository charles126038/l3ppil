﻿#pragma once
#include "COR_FormeChar.h"
#include "../Forme/Segment.h"


/**
 * @brief La partie de la chaine de responsabilite est utilisee pour gerer le cercle
*/
class COR_Forme_Segment : public COR_FormeChar
{


public:
	/**
	 * @brief comme ExpertCOR 4
	 * @param suivant Transmettre la tache à la ExpertCOR suivante
	 * @return rien
	*/
	COR_Forme_Segment(COR_FormeChar* suivant = NULL) :COR_FormeChar(suivant) { }
	~COR_Forme_Segment() {}


protected:
	/**
	 * @brief Separez le string par des virgules et stocker des string dans vector<string>s
	 si s[0]==Forme::SEGMENT return s sinon return null
	 * @param ch le parametre string entree
	 * @return vector<string>s ou null
	*/
	const vector<string> estDetecte(string ch) const {
		vector<string> s;
		vector<string> null; // un vecteur vide
		split(ch, s, ",");
		if (stoi(s[0]) == Forme::SEGMENT)
			return s;
		return null;
	}

	/**
	 * @brief Charger: Mise en oeuvre d'une methode specifique,vector<string> s se transforme en Segment
	 * @param s vector<string>
	 * @return Segment *segment
	*/
	Forme* Charger(vector<string> s) const {
		Segment* segment = new Segment(stod(s[2]), stod(s[3]), stod(s[4]), stod(s[5]));
		segment->setCouleur(stoi(s[1]));
		return segment;
	}

};
