﻿#pragma once
#include "COR_FormeChar.h"
#include "../Forme/Polygone.h"



/**
 * @brief La partie de la chaine de responsabilite est utilisee pour gerer le cercle
*/
class COR_Forme_Polygone : public COR_FormeChar
{


public:
	/**
	 * @brief comme ExpertCOR 3
	 * @param suivant Transmettre la tache à la ExpertCOR suivante
	 * @return rien
	*/
	COR_Forme_Polygone(COR_FormeChar* suivant = NULL) :COR_FormeChar(suivant) { }
	~COR_Forme_Polygone() { }

protected:
	/**
	 * @brief Separez le string par des virgules et stocker des string dans vector<string>s
	 si s[0]==Forme::POLYGONE return s sinon return null
	 * @param ch le parametre string entree
	 * @return vector<string>s ou null
	*/
	const vector<string> estDetecte(string ch) const {
		vector<string> s;
		vector<string> null; // un vecteur vide
		split(ch, s, ",");
		if (stoi(s[0]) == Forme::POLYGONE)
			return s;
		return null;
	}

	/**
	 * @brief Charger: Mise en oeuvre d'une methode specifique,vector<string> s se transforme en Polygone
	 * @param s vector<string>
	 * @return Polygone *polygone
	*/
	Forme* Charger(vector<string> s) const {
		vector<double> d;
		// transformer de vector string a vector double
		for (std::size_t i = 0; i < s.size()-3; i++) {
			d.push_back(stod(s[i+3]));  // a partir de troisieme
		}
		Polygone* polygone = new Polygone(stoi(s[2]), d);
		polygone->setCouleur(stoi(s[1]));
		return polygone;
	}


};
