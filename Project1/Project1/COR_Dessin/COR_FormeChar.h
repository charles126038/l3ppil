#pragma once
#include<stdio.h>
#include<iostream>
#include <string>
#include "FormeChar.h"
#include "../Forme/Forme.h"
#include "ChargerFichier.h"

using namespace std;
/**
 * @brief comme ExpertCOR 
*/
class COR_FormeChar :public FormeChar {
private:
	COR_FormeChar* _suivant;

public:
	COR_FormeChar(COR_FormeChar* suivant = NULL) {
		_suivant = suivant;
	}
	virtual ~COR_FormeChar() {
		free(this->_suivant);
	}

	Forme* detecter(string ch) const {
		vector<string> s = estDetecte(ch);
		if (!s.empty())
			return Charger(s);
		else if (_suivant != NULL)
			return _suivant->detecter(ch);
		else
			return NULL;
	}


protected:
	virtual const vector<string> estDetecte(string ch) const = 0;
	virtual Forme* Charger(vector<string> s) const = 0;

};

