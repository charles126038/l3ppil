﻿#pragma once
#include "COR_FormeChar.h"
#include "../Forme/Composant.h"
#include "COR_Forme_Cercle.h"
#include "COR_Forme_Polygone.h"
#include "COR_Forme_Segment.h"

/**
 * @brief La partie de la chaine de responsabilite est utilisee pour gerer le Composant
*/
class COR_Forme_Composant : public COR_FormeChar
{


public:
	/**
	 * @brief comme ExpertCOR 2
	 * @param suivant Transmettre la tache à la ExpertCOR suivante
	 * @return rien
	*/
	COR_Forme_Composant(COR_FormeChar* suivant = NULL) : COR_FormeChar(suivant) { }


protected:
	/**
	 * @brief Separez le string par des symbole ; ou , et stocker des string dans vector<string> s1
	 * @param ch le parametre string entree
	 * @return vector<string>s1 ou null
	*/
	const vector<string> estDetecte(string ch) const{
		vector<string> s1;
		vector<string> s2;
		vector<string> null; // un vecteur vide
		// format: (type, nombre des formes; type, color, ...; type, color, ...)
		split(ch, s1, ";"); // separer par ';'
		if (s1.size() == 1) // si une composant dans une autre composant, alors les combiner
			return null;
		split(s1[0], s2, ","); // retirer la premiere partie et la separer par ',' (type, nombre des formes)
		if (stoi(s2[0]) == Forme::COMPOSANT)
			return s1;
		return null;
	}

	/**
	 * @brief Charger: Mise en oeuvre d'une methode specifique,vector<string> s se transforme en Composant
	 * @param s vector<string>
	 * @return Composant *composant
	*/
	Forme* Charger(vector<string> s) const {
		Composant* composant = new Composant();

		//construire une chaine de responsabilite
		COR_Forme_Cercle* corC = new COR_Forme_Cercle();
		COR_Forme_Polygone* corP = new COR_Forme_Polygone(corC);
		COR_Forme_Segment* corS = new COR_Forme_Segment(corP);
		COR_Forme_Composant* corCo = new COR_Forme_Composant(corS);

		// creer la forme et l'ajouter dans le composant
		for (std::size_t i = 0; i < s.size()-1; i++) {
			Forme * f = corCo->detecter(s[i + 1]);// a partir de deuxieme element
			if(f)
				composant->add(f);
		}

		return composant;
	}

};
